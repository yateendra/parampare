% -*- Mode: Prolog -*-

% Copyright Yatheendra.M
% All rights reserved

child(_u, _m) :- putra(_u, _m); putri(_u, _m).
maata(_m, _u) :- child(_u, _m).
pita(_p, _u) :- child(_u, _m), patni(_m, _p).
parent(_m-_p, _u) :- pita(_p, _u), child(_u, _m).
bhraata(_b, _u) :- dif(_b, _u), putra(_b, _m), child(_u, _m).
bhagini(_b, _u) :- dif(_b, _u), putri(_b, _m), child(_u, _m).

pitravya(_pv, _u) :- pita(_p, _u), bhraata(_p, _pv).

pitruswasa_pati(_psp, _u) :- pita(_p, _u), bhagini(_ps, _p), patni(_ps, _psp).

swashrum(_jm, _u) :- patni(_b, _u), maata(_jm, _b).

pitaamaha(_pm, _u) :- pita(_pm, _p), pita(_p, _u).
prapitaamaha(_ppm, _u) :- pita(_ppm, _p), pitaamaha(_p, _u).

pitaamahi(_pm, _u) :- maata(_pm, _p), pita(_p, _u).
prapitaamahi(_ppm, _u) :- maata(_ppm, _p), pitaamaha(_p, _u).

maataamaha(_mp, _u) :- pita(_mp, _m), maata(_m, _u).
maatupitaamaha(_mp, _u) :- pitaamaha(_mp, _m), maata(_m, _u).
maatuprapitaamaha(_mp, _u) :- prapitaamaha(_mp, _m), maata(_m, _u).

maataamahi(_mp, _u) :- maata(_mp, _m), maata(_m, _u).
maatupitaamahi(_mp, _u) :- pitaamahi(_mp, _m), maata(_m, _u).
maatuprapitaamahi(_mp, _u) :- prapitaamahi(_mp, _m), maata(_m, _u).
