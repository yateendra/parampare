% -*- Mode: Prolog -*-

% Copyright Yatheendra.M
% All rights reserved

pitru_varga(_tgp, _u) :-
    length(_tgp, 3), last(_tgp, _p), deceased(_p), (deceased(_p1), pita(_p1, _u)),
    gotra(_g, _p), nth0(1, _tgp, _g),
    ((prapitaamaha(_p, _u), nth0(0, _tgp, 'prapitaamaham'));
     (pitaamaha(_p, _u), nth0(0, _tgp, 'pitaamaham'));
     (pita(_p, _u), nth0(0, _tgp, 'pitaram'))).

maatru_varga(_tgm, _u) :-
    length(_tgm, 3), last(_tgm, _m), deceased(_m), (deceased(_m1), maata(_m1, _u)),
    gotra(_g, _m), nth0(1, _tgm, _g),
    ((prapitaamahi(_m, _u), nth0(0, _tgm, 'prapitaamaheem'));
     (pitaamahi(_m, _u), nth0(0, _tgm, 'pitaamaheem'));
     (maata(_m, _u), nth0(0, _tgm, 'maataram'))).

maataamaha_varga(_p, _u) :- deceased(_p), (deceased(_m1), maata(_m1, _u)), (maataamaha(_p, _u); maatupitaamaha(_p, _u); maatuprapitaamaha(_p, _u)).

maataamahi_varga(_p, _u) :- deceased(_p), (deceased(_m1), maata(_m1, _u)), (maataamahi(_p, _u); maatupitaamahi(_p, _u); maatuprapitaamahi(_p, _u)).

anyapitru_varga(_p, _u) :- deceased(_p), (pitravya(_p, _u); pitruswasa_pati(_p, _u); swashrum(_p, _u); aapta(_p, _u)).
